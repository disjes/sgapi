﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Helpers;
using System.Web.Http;
using System.Web.Mvc;
using Newtonsoft.Json;

namespace SGApi.Controllers
{
    public class ScannerController : ApiController
    {
        // GET: api/Scanner
        public Mov_Enc_D GetFactura(string id)
        {
            var SGModel = new SGModel();
            var factura = SGModel.Mov_Enc_D.FirstOrDefault(x => x.Empresa.Trim() == id);
            return factura;            
        }
    }
}
