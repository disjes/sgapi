﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Helpers;
using System.Web.Http;
using System.Web.Mvc;
using Newtonsoft.Json;
using SGApi.Models.SGModel;

namespace SGApi.Controllers
{
    public class SecurityController : ApiController
    {

        // GET: api/Security/CheckDeviceLicense/5
        [System.Web.Http.HttpGet]
        public bool CheckDeviceLicense(string id)
        {
            var db = new SGModel();
            var device = db.Devices.FirstOrDefault(x => x.IdUniqueDevice == id);
            if (device != null)
            {
                if (device.IdUniqueDevice.Trim() == GetGeneratedId(id))
                {
                    AddConexion(device, device.Licencia);
                    return true;                    
                }
                AddConexion(device, "INVALIDO");
            }           
             
            return false;
        }

        private void AddConexion(Devices device, string licencia)
        {
            var db = new SGModel();
            var conexion= new Conexiones() { IdDevice = device.Id, FechaConexion = DateTime.Now, FechaDesconexion = null, LicenciaDevice = licencia };
            db.Conexiones.Add(conexion);
            db.SaveChanges();
        }

        private string GetGeneratedId(string id)
        {
            //TODO: CALL ENCODE FUNCTION AND GET ENCODED RESULT INTO A VARIABLE, THEN RETURN IT
            return id; 
        }

        // POST: api/Security
        public void Post([FromBody]string value)
        {

        }

        // PUT: api/Security/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Security/5
        public void Delete(int id)
        {
        }
    }
}
