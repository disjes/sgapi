﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;

namespace SGApi.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Title = "Holis Home Page";            
            return View();
        }

        public JsonResult GetFactura(string id)
        {
            var SGModel = new SGModel();
            var factura = SGModel.Mov_Enc_D.FirstOrDefault(x => x.Empresa.Trim() == id);

            return Json(factura, JsonRequestBehavior.AllowGet);
        }
    }
}
