using SGApi.Models.SGModel;

namespace SGApi
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class SGModel : DbContext
    {
        public SGModel()
            : base("name=SGModel")
        {
        }

        public virtual DbSet<Conexiones> Conexiones { get; set; }
        public virtual DbSet<Devices> Devices { get; set; }
        public virtual DbSet<Escaneos> Escaneos { get; set; }
        public virtual DbSet<Mov_Enc_D> Mov_Enc_D { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Conexiones>()
                .Property(e => e.LicenciaDevice)
                .IsFixedLength();

            modelBuilder.Entity<Devices>()
                .Property(e => e.DeviceName)
                .IsFixedLength();

            modelBuilder.Entity<Devices>()
                .Property(e => e.IdUniqueDevice)
                .IsFixedLength();

            modelBuilder.Entity<Devices>()
                .Property(e => e.Licencia)
                .IsFixedLength();

            modelBuilder.Entity<Devices>()
                .HasMany(e => e.Conexiones)
                .WithOptional(e => e.Devices)
                .HasForeignKey(e => e.IdDevice);

            modelBuilder.Entity<Devices>()
                .HasMany(e => e.Escaneos)
                .WithOptional(e => e.Devices)
                .HasForeignKey(e => e.IdDevice);

            modelBuilder.Entity<Escaneos>()
                .Property(e => e.BarCodeRead)
                .IsUnicode(false);

            modelBuilder.Entity<Mov_Enc_D>()
                .Property(e => e.Neto)
                .HasPrecision(19, 4);

            modelBuilder.Entity<Mov_Enc_D>()
                .Property(e => e.Descuento)
                .HasPrecision(19, 4);

            modelBuilder.Entity<Mov_Enc_D>()
                .Property(e => e.DescuentoP)
                .HasPrecision(19, 4);

            modelBuilder.Entity<Mov_Enc_D>()
                .Property(e => e.Descuento_2)
                .HasPrecision(19, 4);

            modelBuilder.Entity<Mov_Enc_D>()
                .Property(e => e.P_Descuento2)
                .HasPrecision(19, 4);

            modelBuilder.Entity<Mov_Enc_D>()
                .Property(e => e.Descuento_3)
                .HasPrecision(19, 4);

            modelBuilder.Entity<Mov_Enc_D>()
                .Property(e => e.P_Descuento3)
                .HasPrecision(19, 4);

            modelBuilder.Entity<Mov_Enc_D>()
                .Property(e => e.Descuento_4)
                .HasPrecision(19, 4);

            modelBuilder.Entity<Mov_Enc_D>()
                .Property(e => e.P_Descuento4)
                .HasPrecision(19, 4);

            modelBuilder.Entity<Mov_Enc_D>()
                .Property(e => e.Impuesto1)
                .HasPrecision(19, 4);

            modelBuilder.Entity<Mov_Enc_D>()
                .Property(e => e.Impuesto2)
                .HasPrecision(19, 4);

            modelBuilder.Entity<Mov_Enc_D>()
                .Property(e => e.Total)
                .HasPrecision(19, 4);

            modelBuilder.Entity<Mov_Enc_D>()
                .Property(e => e.TotalCosto)
                .HasPrecision(19, 4);

            modelBuilder.Entity<Mov_Enc_D>()
                .Property(e => e.Total_Recibido)
                .HasPrecision(19, 4);

            modelBuilder.Entity<Mov_Enc_D>()
                .Property(e => e.Servicio)
                .HasPrecision(19, 4);

            modelBuilder.Entity<Mov_Enc_D>()
                .Property(e => e.Tasa)
                .HasPrecision(19, 4);
        }
    }
}
