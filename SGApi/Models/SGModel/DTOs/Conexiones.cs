namespace SGApi.Models.SGModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Conexiones
    {
        [Key]
        public int IdConexion { get; set; }

        public int? IdDevice { get; set; }

        public DateTime? FechaConexion { get; set; }

        public DateTime? FechaDesconexion { get; set; }

        [StringLength(60)]
        public string LicenciaDevice { get; set; }

        public virtual Devices Devices { get; set; }
    }
}
