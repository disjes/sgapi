namespace SGApi.Models.SGModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Devices
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Devices()
        {
            Conexiones = new HashSet<Conexiones>();
            Escaneos = new HashSet<Escaneos>();
        }

        public int Id { get; set; }

        [StringLength(20)]
        public string DeviceName { get; set; }

        [StringLength(50)]
        public string IdUniqueDevice { get; set; }

        public DateTime? FechaCreacion { get; set; }

        public bool? Estatus { get; set; }

        [StringLength(60)]
        public string Licencia { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Conexiones> Conexiones { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Escaneos> Escaneos { get; set; }
    }
}
