namespace SGApi.Models.SGModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Escaneos
    {
        [Key]
        public int IdEscaneo { get; set; }

        public int? IdDevice { get; set; }

        [StringLength(150)]
        public string BarCodeRead { get; set; }

        public DateTime? FechaHora { get; set; }

        public virtual Devices Devices { get; set; }
    }
}
