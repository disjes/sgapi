namespace SGApi
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Mov_Enc_D
    {
        [StringLength(50)]
        public string N_Prefactura { get; set; }

        [StringLength(50)]
        public string Movimiento { get; set; }

        [StringLength(50)]
        public string Empresa { get; set; }

        [StringLength(10)]
        public string Referencia { get; set; }

        [StringLength(50)]
        public string Nombre { get; set; }

        [StringLength(50)]
        public string RTN { get; set; }

        [StringLength(100)]
        public string Direccion { get; set; }

        [StringLength(50)]
        public string Telefono { get; set; }

        [StringLength(10)]
        public string Orden { get; set; }

        public DateTime? Fecha { get; set; }

        public DateTime? Vencimiento { get; set; }

        [StringLength(50)]
        public string Observacion1 { get; set; }

        [StringLength(50)]
        public string Observacion2 { get; set; }

        [Column(TypeName = "money")]
        public decimal? Neto { get; set; }

        [Column(TypeName = "money")]
        public decimal? Descuento { get; set; }

        [Column(TypeName = "money")]
        public decimal? DescuentoP { get; set; }

        [Column(TypeName = "money")]
        public decimal? Descuento_2 { get; set; }

        [Column(TypeName = "money")]
        public decimal? P_Descuento2 { get; set; }

        [Column(TypeName = "money")]
        public decimal? Descuento_3 { get; set; }

        [Column(TypeName = "money")]
        public decimal? P_Descuento3 { get; set; }

        [Column(TypeName = "money")]
        public decimal? Descuento_4 { get; set; }

        [Column(TypeName = "money")]
        public decimal? P_Descuento4 { get; set; }

        [Column(TypeName = "money")]
        public decimal? Impuesto1 { get; set; }

        [Column(TypeName = "money")]
        public decimal? Impuesto2 { get; set; }

        [Column(TypeName = "money")]
        public decimal? Total { get; set; }

        [Column(TypeName = "money")]
        public decimal? TotalCosto { get; set; }

        [Key]
        [Column(Order = 0)]
        public bool Nula { get; set; }

        public int? Usuario { get; set; }

        [StringLength(20)]
        public string TipoVenta { get; set; }

        [StringLength(20)]
        public string Carnet { get; set; }

        [Column(TypeName = "money")]
        public decimal? Total_Recibido { get; set; }

        [Column(TypeName = "money")]
        public decimal? Servicio { get; set; }

        [StringLength(50)]
        public string Habitacion { get; set; }

        [StringLength(100)]
        public string Huesped { get; set; }

        public int? Codigo_Empleado { get; set; }

        [StringLength(50)]
        public string Mesa { get; set; }

        public int? Personas { get; set; }

        [Key]
        [Column(Order = 1)]
        public bool Cerrada { get; set; }

        [StringLength(50)]
        public string Hora { get; set; }

        [Key]
        [Column(Order = 2)]
        public bool Es_Factura { get; set; }

        public int? Tipo_Orden { get; set; }

        public int? Hora_2 { get; set; }

        [Key]
        [Column(Order = 3)]
        public bool Es_Credito { get; set; }

        [Key]
        [Column(Order = 4)]
        public bool Paga_Impto { get; set; }

        public bool? Seleccionada { get; set; }

        public bool? Impresa { get; set; }

        public int? ID_Mesero { get; set; }

        public int? ID_Tipo_Numeracion { get; set; }

        public int? ID_Tipo_Cortesia { get; set; }

        public int? ID_Centro_Costos { get; set; }

        public int? ID_Departamento { get; set; }

        [Column(TypeName = "money")]
        public decimal? Tasa { get; set; }

        [StringLength(50)]
        public string Orden_Compra { get; set; }

        [StringLength(150)]
        public string Total_Letras { get; set; }
    }
}
